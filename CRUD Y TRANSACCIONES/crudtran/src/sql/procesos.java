package sql;

import getset.variables;
import java.sql.Connection;
import javax.swing.JOptionPane;
import java.sql.ResultSet;


public class procesos extends coneccionpostgrest{
    
    java.sql.Statement st;
    ResultSet rs;
    variables var= new variables();
   
    /*CRUD1 TABLA POSTULANTES EJERCITO*/ 
    
   public void insertar( String nombres, String apellidos, String fechaNac) {
        try {
            Connection conexion =  Conectar();
            st = conexion.createStatement();
            String sql = "Insert into postulantes_ejercito(nombre_postulante, apellido_postulante, fecha_nacimiento)" 
                    + "values('" + nombres + "','" + apellidos + "','" + fechaNac + "');";
            st.execute(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "El registro se guardo correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El registro no se guardo" + e, "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
    }
    
   public void consultar(String id){
        try {
            Connection conexion =Conectar();
            st = conexion.createStatement();
            String sql="select * from postulantes_ejercito where id_postulante='"+id+"'";
            rs=st.executeQuery(sql);
            if (rs.next()) 
            {
                var.setId_postulante(rs.getString("id_postulante"));
                var.setNombre_postulante(rs.getString("nombre_postulante"));
                var.setApellido_postulante(rs.getString("apellido_postulante"));
                var.setFecha_nacimiento(rs.getString("fecha_nacimiento"));
            }else{
                var.setId_postulante(rs.getString(""));
                var.setNombre_postulante(rs.getString(""));
                var.setApellido_postulante(rs.getString(""));
                var.setFecha_nacimiento(rs.getString(""));
                JOptionPane.showMessageDialog(null, "No se encontro registro","Sin registro",JOptionPane.INFORMATION_MESSAGE);
            }
            st.close();
            conexion.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error en el sistema de busqueda"+e,"Error buscqueda",JOptionPane.ERROR_MESSAGE);
            
                var.setId_postulante("");
                var.setNombre_postulante("");
                var.setApellido_postulante("");
                var.setFecha_nacimiento("");
        } 
    } 
   
   public void modificar(String id, String nombres, String apellidos, String fechaNac){
        try {
             Connection conexion =Conectar();
             st=conexion.createStatement();
             String sql="update postulantes_ejercito set nombre_postulante='"+nombres+"',apellido_postulante='"+apellidos+"',"
                     + "fecha_nacimiento='"+fechaNac+"' where id_postulante='"+id+"'";
             st.executeUpdate(sql);
             JOptionPane.showMessageDialog(null, "El registro fue modificado correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
             st.close();
            conexion.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El registro no se modifico" + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
   
   public void eliminar(String id){
        try {
            Connection conexion =Conectar();
            st=conexion.createStatement();
            String sql="delete from postulantes_ejercito where id_postulante='"+id+"' ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "El registro fue eliminado correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El registro no se Elimino " + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
   
      /*CRUD2 TABLA MISIONES EJERCITO*/ 
   
      public void insertarCRUD2( String tipo, String lider, String instrumento, String estado, String inicio, String fin) {
        try {
            Connection conexion =  Conectar();
            st = conexion.createStatement();
            String sql = "Insert into misiones_ejercito(tipo_mision, lider_grupo_misiones, instrumentos_militares_usados, "
                    + "estado_instrumento_militar, fecha_inicio_mision, fecha_fin_mision)" 
                    + "values('" + tipo + "','" + lider + "','" + instrumento + "','" + estado + "','" + inicio + "','" + fin + "');";
            st.execute(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "El registro se guardo correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El registro no se guardo" + e, "Mensaje", JOptionPane.ERROR_MESSAGE);
        }
      }

      public void consultarCRUD2(String id){
        try {
            Connection conexion =Conectar();
            st = conexion.createStatement();
            String sql="select * from misiones_ejercito where id_mision='"+id+"'";
            rs=st.executeQuery(sql);
            if (rs.next()) 
            {
                var.setId_mision(rs.getString("id_mision"));
                var.setTipo_mision(rs.getString("tipo_mision"));
                var.setLider_grupo_misiones(rs.getString("lider_grupo_misiones"));
                var.setInstrumentos_militares_usados(rs.getString("instrumentos_militares_usados"));
                var.setEstado_instrumento_militar(rs.getString("estado_instrumento_militar"));
                var.setFecha_inicio_mision(rs.getString("fecha_inicio_mision"));
                var.setFecha_fin_mision(rs.getString("fecha_fin_mision"));   
            }else{
                var.setId_mision(rs.getString(""));
                var.setTipo_mision(rs.getString(""));
                var.setLider_grupo_misiones(rs.getString(""));
                var.setInstrumentos_militares_usados(rs.getString(""));
                var.setEstado_instrumento_militar(rs.getString(""));
                var.setFecha_inicio_mision(rs.getString(""));
                var.setFecha_fin_mision(rs.getString(""));
                JOptionPane.showMessageDialog(null, "No se encontro registro","Sin registro",JOptionPane.INFORMATION_MESSAGE);
            }
            st.close();
            conexion.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error en el sistema de busqueda"+e,"Error buscqueda",JOptionPane.ERROR_MESSAGE);
                
                var.setId_mision("");
                var.setTipo_mision("");
                var.setLider_grupo_misiones("");
                var.setInstrumentos_militares_usados("");
                var.setEstado_instrumento_militar("");
                var.setFecha_inicio_mision("");
                var.setFecha_fin_mision("");       
        } 
    } 
   
      public void modificarCRUD2(String id, String tipo, String lider, String instrumento, String estado, String inicio, String fin){
        try {
             Connection conexion =Conectar();
             st=conexion.createStatement();
             String sql="update misiones_ejercito set tipo_mision='"+tipo+"',lider_grupo_misiones='"+lider+"',instrumentos_militares_usados='"+instrumento+"',"
                     + "estado_instrumento_militar='"+estado+"',fecha_inicio_mision='"+inicio+"',fecha_fin_mision='"+fin+"'"
                     + " where id_mision='"+id+"'";
             st.executeUpdate(sql);
             JOptionPane.showMessageDialog(null, "El registro fue modificado correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
             st.close();
            conexion.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El registro no se modifico" + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
   
     public void eliminarCRUD2(String id){
        try {
            Connection conexion =Conectar();
            st=conexion.createStatement();
            String sql="delete from misiones_ejercito where id_mision='"+id+"' ";
            st.executeUpdate(sql);
            st.close();
            conexion.close();
            JOptionPane.showMessageDialog(null, "El registro fue eliminado correctamente", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El registro no se Elimino " + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
}
