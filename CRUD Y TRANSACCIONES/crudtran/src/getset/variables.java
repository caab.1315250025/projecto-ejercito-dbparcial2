package getset;

public class variables {
    
    /*Variables para CRUD 1 Postulantes Ejercito*/
    private static String id_postulante;
    private static String nombre_postulante;
    private static String apellido_postulante;
    private static String fecha_nacimiento;
    
    public static String getId_postulante() {
        return id_postulante;
    }

    public static void setId_postulante(String id_postulante) {
        variables.id_postulante = id_postulante;
    }

    public static String getNombre_postulante() {
        return nombre_postulante;
    }

    public static void setNombre_postulante(String nombre_postulante) {
        variables.nombre_postulante = nombre_postulante;
    }

    public static String getApellido_postulante() {
        return apellido_postulante;
    }

    public static void setApellido_postulante(String apellido_postulante) {
        variables.apellido_postulante = apellido_postulante;
    }

    public static String getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public static void setFecha_nacimiento(String fecha_nacimiento) {
        variables.fecha_nacimiento = fecha_nacimiento;
    }
    
    
   /*Variables para CRUD 2 Misiones Ejercito*/

    private static String id_mision;
    private static String tipo_mision; 
    private static String lider_grupo_misiones; 
    private static String instrumentos_militares_usados; 
    private static String estado_instrumento_militar; 
    private static String fecha_inicio_mision; 
    private static String fecha_fin_mision;

     
    public static String getId_mision() {
        return id_mision;
    }

    public static void setId_mision(String id_mision) {
        variables.id_mision = id_mision;
    }
    
    public static String getTipo_mision() {
        return tipo_mision;
    }

    public static void setTipo_mision(String tipo_mision) {
        variables.tipo_mision = tipo_mision;
    }

    public static String getLider_grupo_misiones() {
        return lider_grupo_misiones;
    }

    public static void setLider_grupo_misiones(String lider_grupo_misiones) {
        variables.lider_grupo_misiones = lider_grupo_misiones;
    }

    public static String getInstrumentos_militares_usados() {
        return instrumentos_militares_usados;
    }

    public static void setInstrumentos_militares_usados(String instrumentos_militares_usados) {
        variables.instrumentos_militares_usados = instrumentos_militares_usados;
    }

    public static String getEstado_instrumento_militar() {
        return estado_instrumento_militar;
    }

    public static void setEstado_instrumento_militar(String estado_instrumento_militar) {
        variables.estado_instrumento_militar = estado_instrumento_militar;
    }

    public static String getFecha_inicio_mision() {
        return fecha_inicio_mision;
    }

    public static void setFecha_inicio_mision(String fecha_inicio_mision) {
        variables.fecha_inicio_mision = fecha_inicio_mision;
    }

    public static String getFecha_fin_mision() {
        return fecha_fin_mision;
    }

    public static void setFecha_fin_mision(String fecha_fin_mision) {
        variables.fecha_fin_mision = fecha_fin_mision;
    }
}
