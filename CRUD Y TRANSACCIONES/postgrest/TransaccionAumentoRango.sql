create or replace procedure transaccion_AumentoRango(int,int,int,int, varchar)
as
$$
declare
	x int;
	begin
			insert into pruebas_de_rango(id_integrante,id_rango,pruebas_fisicas,pruebas_teoricas,cumple_condicion) values($1,$2,$3,$4,$5);	
			select id_rango into x from integrante_ejercito where id_integrante=$1;
			if (x>=$2) then
				raise exception 'No se puede realizar una prueba de rango menor o igual al rango actual';
				rollback;
			end if;
			if ($3>=7 and $4>=7) then
				if ($5='si') then
				update integrante_ejercito set id_rango=$2 where id_integrante=$1;
				else 
				raise exception 'Para subir de Rango debe cumplir todas las condiciones';
				rollback;
				end if;	
			else 
			raise exception 'Para subir de Rango la nota minima es 7';
			rollback;
			end if;
			
	exception
		when sqlstate '23514' then
		raise exception 'No se existe una id de rango menor a 1 o superior a 17';
		rollback;
	commit;
end;
$$
language plpgsql;
select * from integrante_ejercito

call transaccion_AumentoRango(1,3,7,7,'si')
alter table pruebas_de_rango add check(id_rango>='1' and id_rango<='17');


