create or replace procedure transaccion_Castigo(int,int,date,date,date, varchar)
as
$$
declare
	x int;
	y int;
	z int;
	begin
			insert into castigo_ejercito(id_integrante, id_superior, fecha_incidente_castigo, fecha_inicio_castigo, fecha_fin_castigo, motivo_castigo) 
			values($1,$2,$3,$4,$5,$6);	
			if ($3>$4) then
				raise exception 'La fecha incidente del castigo debe ser antes o el mismo dia del incio del castigo ';
				rollback;
			end if;
			if ($4>=$5) then
				raise exception 'No se puede realizar un castigo si la fecha de inicio es mayor a la fecha de fin';
				rollback;
			end if;
			select id_integrante into x from superior_ejercito where id_superior=$2;
			select id_rango into y from integrante_ejercito where id_integrante=x;
			if (y>=14)then
				select count (*) into z from castigo_ejercito where id_integrante=$1;
				update integrante_ejercito set cantidad_castigos=z where id_integrante=$1;
			else
				raise exception 'Solo los Generales pueden impartir castigos';
				rollback;
			end if;
	exception
		when sqlstate '23514' then
		raise exception 'No se existe una id de un supeior menor a 1 o superior a 3';
		rollback;
	commit;
end;
$$
language plpgsql;

call transaccion_Castigo(17,4,'09/01/2022','09/01/2022','10/01/2022','Indisciplina')
insert into superior_ejercito values (4,12,'abc','dfe')
alter table castigo_ejercito add check(id_superior>='1' and id_superior<='3');

