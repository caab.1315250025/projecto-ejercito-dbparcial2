const functions = require("firebase-functions");
const admin = require("firebase-admin");

admin.initializeApp();

const db = admin.firestore();
exports.onUserCreate = functions.firestore
    .document("Retiro/{userId}")
    .onCreate(async (snap, context) => {
        const soldado = snap.data();
        let query = db.collection("Line_Misiones").where("id_integrante", "==", soldado.id_integrante);
        await query.get().then((snap) => {
            size = snap.size;
        });
        console.log(size);
        const integrante = await db
                .collection("Integrante_Ejercito")
                .where("id_integrante", "==", soldado.id_integrante);
        if (size >= 1) {
            let query = db.collection("Retiro").doc(snap.id).delete()
            console.log(`El Soldado #${soldado.id_integrante} no puede retirarse por que sigue en alguna mision`);
        } else {
            console.log(`El Soldado #${soldado.id_integrante} se retiro exitosamente`);
        }

    });